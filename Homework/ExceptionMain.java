import java.util.Scanner;

import java.util.Scanner;
import java.util.InputMismatchException;
import java.lang.ArithmeticException; //I used lang library to get Arithmetic Exception.


/**
 * 
 */

/**
 * This program is used to alert user to any exceptions. 
 * @author sauzanya
 *
 */
public class ExceptionMain {

	private static final String DIVIDE_BY_ZERO = "You can't divide by zero. Please run the program again.";
	private static final String INVALID_NUMBER = "You entered an invalid number (remember they have to be integers). Please run the program again.";

	/**
	 * Main method for the program.
	 * @param args
	 */
	public static void main(String[] args)
	{
	//Using try and catch	
		try
		{
		Scanner scanner = new Scanner(System.in);
		//Scanner method
		System.out.println("Welcome to the division program.");
		
		System.out.println("Enter the first number (numerator) as an integer");
		//Asks the user for numerator.
		int numerator = scanner.nextInt();
		System.out.println("Enter the second number (denominator) as an integer");
		//Asks the user for denominator.
		int denominator = scanner.nextInt();
				
		double quotient = numerator/denominator;
		//gets the question
		System.out.println("The quotient is " + quotient);
		}catch (InputMismatchException invalid)
		{
			System.out.println(INVALID_NUMBER);
			//prints the error message for input mismatch
		}
		catch (ArithmeticException ae)
		{
			System.out.println(DIVIDE_BY_ZERO);
			//prints the error message for arithmetic error.
		}
		
	}

}
